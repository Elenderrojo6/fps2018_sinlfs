﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class AmmoManager : MonoBehaviour
{
    
    [SerializeField] Text ammo;
 
    private int scoreInt;

    
    // Start is called before the first frame update
    void Start()
    {
         scoreInt = 10;
        ammo.text = scoreInt.ToString("00");
    }
 
    public void AddScore(int value){
        scoreInt-=value;
        ammo.text = scoreInt.ToString("00");
    }
   
   
    
    
}